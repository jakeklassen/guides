# Guides

## How to Generate a CSR for UCC/SAN SSL and Configure an AWS Load Balancer to Use It

First things first follow this guide (You can stop at step 8):

[Creating SSL SAN certificates and CSRs using OpenSSL](https://support.f5.com/kb/en-us/solutions/public/11000/400/sol11438.html)

At this point run the following command to create a PEM output of your `*.key` file:

Kudos: Jonathon Hill answer at http://stackoverflow.com/questions/6753619/how-do-i-setup-ssl-certs-on-amazon-aws-load-balancer-using-godaddy-certificates
```bash
$ openssl rsa -in yourdomain.key -outform PEM -out yourdomain.pem
```

At this point you will have a `*.csr` file that you need to submit to GoDaddy.

Once generation is complete download the zip file provided by GoDaddy and extract it somewhere.

Login to the AWS console and:

1. Select EC2
2. Under Network & Security select Load Balancers
3. Click on the load balancer you need to add a certificate to
4. Select the Listeners tab
5. Change an existing SSL or choose Edit to create an entry
6. When you see the form:
	1. Copy the contents of the `*.pem` file into the private key box
    2. From the GoDaddy files, copy the contents of the `randomId.crt` file into the public key box
    3. From the GoDaddy files, copy the content of the `gd_bundle*.crt` file into the keychain box
7. Save

This should be all you need to do.

# Amazon Linux

Common tasks

## Install nvm

Follow step on [nvm](https://github.com/creationix/nvm) site to install nvm.

If `nvm` is not available, run `source ~/.bashrc`

```bash
$ nvm install 0.10.36 # substitute your node version
```

## Stop SSH Timeout

```bash
$ sudo nano /etc/ssh/sshd_config
TCPKeepAlive yes
ClientAliveInterval 30
ClientAliveCountMax 99999

$ sudo service sshd restart
```

## Packages

```bash
$ sudo yum -y install gcc gcc-c++ make git
```

## Mongodb

From ec2 box:

```bash
$ sudo yum -y update

$ echo "[MongoDB]
name=MongoDB Repository
baseurl=http://downloads-distro.mongodb.org/repo/redhat/os/x86_64
gpgcheck=0
enabled=1" | sudo tee -a /etc/yum.repos.d/mongodb.repo

$ sudo yum install -y mongodb-org-server mongodb-org-shell mongodb-org-tools

$ sudo mkdir -p /data/db

# HALT! This command can take a bit to preallocate space, be patient
$ sudo service mongod start

$ mongo  # should allow you to connect
```

## Redis

```bash
$ sudo -s

$ cd /usr/local/src

$ wget http://download.redis.io/redis-stable.tar.gz
$ tar xvzf redis-stable.tar.gz
$ cd redis-stable
$ make # takes some time

$ mkdir /etc/redis /var/redis
$ cp src/redis-server src/redis-cli /usr/local/bin

$ cp redis.conf /etc/redis/6379.conf

$ mkdir /var/redis/6379

$ nano /etc/redis/6379.conf
# Set daemonize to yes (by default it is set to no).
# Set pidfile to /var/run/redis.pid
# Set preferred loglevel
# Set logfile to /var/log/redis_6379.log
# Set dir to /var/redis/6379

# Don’t copy the standard Redis init script from utils directory into /etc/init.d
# (as it’s not Amazon Linux AMI/chkconfig compliant), instead download the following:
$ wget https://raw.githubusercontent.com/saxenap/install-redis-amazon-linux-centos/master/redis-server

$ mv redis-server /etc/init.d
$ chmod 755 /etc/init.d/redis-server

$ nano /etc/init.d/redis-server
# Set REDIS_CONF_FILE=”/etc/redis/6379.conf”

$ chkconfig --add redis-server
$ chkconfig --level 345 redis-server on
$ service redis-server start

$ nano /etc/sysctl.conf
# sysctl vm.overcommit_memory=1

# Test
$ /usr/local/bin/redis-cli ping
```

# S3 Static Website

[Static website on S3, CloudFront and Route 53](http://www.michaelgallego.fr/blog/2013/08/27/static-website-on-s3-cloudfront-and-route-53-the-right-way/)

# SSL

[AWS CloundFront/S3 SSL](https://bryce.fisher-fleig.org/blog/setting-up-ssl-on-aws-cloudfront-and-s3/)

[CSR Generator](https://www.openprovider.co.uk/ssl-certificates/csr-generation-tool/)

[openssl UCC CSR](https://rtcamp.com/wordpress-nginx/tutorials/ssl/multidomain-ssl-subject-alternative-names/)

[GoDaddy AWS SSL](http://knackforge.com/blog/vishnu/aws-elastic-load-balancer-godaddy-ssl)

# cURL

Some useful commands

```shell
# Display some useful times against a route
# Example query against the Urban Arabia API
curl -IL --insecure -s -w %{time_connect}:%{time_starttransfer}:%{time_total}\\n \
https://dev.api.urbanarabia.com/v1/property/54d5370b9f0e7f0e497bf426/users \
--header "Authorization: Bearer IMqEUDaGlFZptoNo1iLXFtlc0fCvlpk8bSyw7ThbhYo="
```

# Linux

Reroute traffic:

```bash
sudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-ports 3000
```
